import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Item } from '../api.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notificationList: Item[] = [];

  notificationData$: Subject<Item[]> = new Subject();

  constructor() {

  }

  /**
   * Notification Stream
   * @returns Observable stream for Items 
   */
  listenNotification() {
    return this.notificationData$.asObservable();
  }


  /**
   * Update NotificationList based on selected with below params
   * @param item selected issue
   * @param addorRemove flag to add/remove item from notificationlist
   * @param allSelect flag add/remove All items in notification
   * @param list contains all issue list
   */
  updateNotification(item: Item, addorRemove: boolean, allSelect?: boolean, list?: Item[]) {
    if (allSelect) {
      this.notificationList = list.map(item => ({ ...item }));
    }
    else if (allSelect === false) {
      this.notificationList = [];
    } else {
      const foundAT = this.notificationList.findIndex(listItem => listItem.id === item.id);
      if (addorRemove && foundAT === -1) {
        this.notificationList.push(item);
      } else {
        !addorRemove && this.notificationList.splice(foundAT, 1);
      }
    }
    this.publishNotifications();

  }

  /**
   * Push notification changes
   */
  publishNotifications() {
    this.notificationData$.next(this.notificationList);
  }

}
