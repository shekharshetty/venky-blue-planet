import { Injectable, Injector } from '@angular/core';
import { HttpParams, HttpHeaders, HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface ConnectOptions {
  url: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'OPTIONS';
  headers?: HttpHeaders;
  responseType?: 'arraybuffer' | 'json' | 'blob' | 'text';
  params?: HttpParams;
  body?: any;
}

@Injectable({
  providedIn: 'root'
})
export class ConnectApiService {

  constructor(private injector: Injector) {

  }

  /**
   * fetching http service instance from Injector
   */
  public get http() {
    return this.injector.get(HttpClient);
  }

  /**
   *
   * @param options connectionOptions object with method and url
   * @returns Observable
   */
  public connect(options: ConnectOptions): Observable<any> {

    const reqObj: HttpRequest<any> = new HttpRequest<any>(options.method, options.url, options.body, {
      headers: options?.headers,
      params: options.params || null
    });

    return this.http.request(reqObj).pipe(
      filter(res => res.type !== 0),
      map((res: HttpResponse<any>) => res.body)
    );

  }

}
