import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConnectApiService } from './services/connect-api.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {

  let component: AppComponent;
  let debugElm: DebugElement;
  let fixture: ComponentFixture<AppComponent>;
  let apiService: ConnectApiService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [ConnectApiService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    debugElm = fixture.debugElement;
    apiService = TestBed.inject(ConnectApiService);
    fixture.detectChanges();
  });


  it('should create component with dependencies', () => {
    expect(component).toBeTruthy();
    expect(apiService).toBeTruthy();
  });

  it('should display header', () => {
    expect(debugElm.query(By.css('.header'))).not.toBeNull();
  });

});
