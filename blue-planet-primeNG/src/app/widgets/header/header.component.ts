import { Component, OnDestroy, OnInit } from '@angular/core';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { Item } from 'src/app/api.model';
import { NotificationService } from 'src/app/services/notification.service';
import { Message } from 'primeng/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [ConfirmationService]
})
export class HeaderComponent implements OnInit, OnDestroy {

  items: MenuItem[];
  notificationCount = 0;
  selectedItem: Item;
  issueList: Item[] = [];
  cols: any[];
  msgs: Message[] = [];
  subscription: Subscription;

  constructor(private notificationService: NotificationService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.items = [];
    this.cols = [
      { field: 'condition-severity', header: 'Severity' },
      { field: 'node-id', header: 'Description' },
      { field: 'node-type', header: 'Node Type' }
    ];
    this.listenNotification();
  }

  /**
   * Subsciribe to Notification service to fetch selected issues
   */
  listenNotification() {
    this.subscription = this.notificationService.listenNotification().subscribe(value => {
      this.notificationCount = value.length;
      this.issueList = value;
    });
  }

  /**
   * Signout action to show confirmation popup
   */
  signOut() {
    this.confirmationService.confirm({
      message: 'Sign Out of Blue Planet?',
      accept: () => {
        this.msgs = [{ summary: 'Signout', detail: 'Thanks for visiting this Page' }];
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }
}
