
import { Component, OnInit } from '@angular/core';
import { MenuItem, Message } from 'primeng/api';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  menuItems: MenuItem[];
  display: boolean = false;
  actionMessage: Message[] = [];

  constructor() {
    this.getMenuItems();
  }

  ngOnInit(): void {

  }

  handleClick(event) {
    console.log('btn clicked');
  }

  getMenuItems() {
    this.menuItems = [{
      items: [
        {
          label: 'Create',
          command: () => {
            this.menuAction('create');
          }
        }, {
          label: 'Update',
          command: () => {
            this.menuAction('update');
          }
        },
        {
          label: 'Delete',
          command: () => {
            this.menuAction('delete');
          }
        }
      ]
    }
    ];
  }


  menuAction(action: string) {
    this.display = true;
    action = (action.charAt(0).toUpperCase() + action.slice(1));
    this.actionMessage = [{ summary: action, detail: 'has been Activated' }];
    setTimeout(() => {
      this.display = false;
    }, 2000);
  }
}
