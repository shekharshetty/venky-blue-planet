import { Component, Input, OnInit } from '@angular/core';
import { Item, TableHeader } from 'src/app/api.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-bp-table',
  templateUrl: './bp-table.component.html',
  styleUrls: ['./bp-table.component.scss']
})
export class BpTableComponent implements OnInit {

  @Input('list') issueList: Item[];
  @Input('header-list') cols: TableHeader[];
  @Input() highlightSeverity = false;

  selectedItems: Item[];

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void { }

  onRowSelect(item : Item) {
    this.notificationService.updateNotification(item, true);
  }

  onRowUnselect(item) {
    this.notificationService.updateNotification(item, false);
  }

  allRowSelect(event) {
    this.notificationService.updateNotification(event, true, event.checked, this.issueList);
  }

}
