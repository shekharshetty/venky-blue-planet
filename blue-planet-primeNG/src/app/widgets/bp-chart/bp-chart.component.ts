import { Component, Input, OnInit } from '@angular/core';
import { Facets, SeverityType } from 'src/app/api.model';
@Component({
  selector: 'app-bp-chart',
  templateUrl: './bp-chart.component.html',
  styleUrls: ['./bp-chart.component.scss']
})
export class BpChartComponent implements OnInit {

  data: any;
  @Input() facets: Facets;

  constructor() {
    this.data = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [],
          hoverBackgroundColor: []
        }]
    };
  }

  ngOnInit(): void {
    this.facets['condition-severity'].forEach(item => {
      this.data.labels.push(item.key);
      this.data.datasets[0].data.push(item.count);
      this.data.datasets[0].backgroundColor.push(this.getSeverityColorByType(item.key) );
      this.data.datasets[0].hoverBackgroundColor.push(this.getSeverityColorByType(item.key)+ '9f');
    });
  }

  getSeverityColorByType(type: SeverityType) {
    const colorMap = {
      critical: '#e71d1d',
      major: '#ef8e00',
      minor: '#ddd326'
    };
    return colorMap[type.toLowerCase()];
  }

}
