import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BpChartComponent } from './bp-chart.component';

describe('BpChartComponent', () => {
  let component: BpChartComponent;
  let fixture: ComponentFixture<BpChartComponent>;

  let fakeFacets = {
    'condition-severity': [
      {
        key: 'CRITICAL',
        count: 1
      },
      {
        key: 'MAJOR',
        count: 5
      },
      {
        key: 'MINOR',
        count: 1
      }
    ]
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BpChartComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BpChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
