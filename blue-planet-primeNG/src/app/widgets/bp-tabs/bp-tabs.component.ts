import { Component, Input, OnInit } from '@angular/core';
import { Item, TableHeader } from 'src/app/api.model';

@Component({
  selector: 'app-bp-tabs',
  templateUrl: './bp-tabs.component.html',
  styleUrls: ['./bp-tabs.component.scss']
})
export class BpTabsComponent implements OnInit {

  nodeTypeList: Item[] = [];
  criticalList: Item[] = [];
  majorList: Item[] = [];
  tableHeader: TableHeader[];

  @Input() alarmList: Item[];

  constructor() {
    this.setTableHeader();
  }

  ngOnInit(): void {
    this.setTableList();
  }

  setTableHeader() {
    this.tableHeader = [
      { field: 'node-type', header: 'Node Type' },
      { field: 'manual-clearable', header: 'Clearable' },
      { field: 'condition-severity', header: 'Severity' },
      { field: 'state', header: 'State' },
      { field: 'last-raise-time', header: 'Raise Time' }
    ];
  }

  setTableList() {
    this.criticalList = this.alarmList.filter(item => item['condition-severity'] === 'CRITICAL');
    this.majorList = this.alarmList.filter(item => item['condition-severity'] === 'MAJOR');
    this.nodeTypeList = this.alarmList.map(ele => ele).sort((a, b) => a['node-type'].localeCompare(b['node-type']));
  }

}
