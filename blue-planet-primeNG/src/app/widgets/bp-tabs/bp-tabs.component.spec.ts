import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BpTableComponent } from '../bp-table/bp-table.component';

import { BpTabsComponent } from './bp-tabs.component';

fdescribe('BpTabsComponent', () => {
  let component: BpTabsComponent;
  let fixture: ComponentFixture<BpTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BpTabsComponent ,  BpTableComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BpTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
