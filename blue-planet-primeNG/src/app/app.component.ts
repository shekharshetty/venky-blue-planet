import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Facets, Issue, Item, TableHeader } from './api.model';
import { ConnectApiService } from './services/connect-api.service';
import { Message } from 'primeng/api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  issueFacets: Facets;
  issueList: Item[];
  selectedItems: Item[];

  cols: any[];
  msgs: Message[] = [];
  isAjaxSuccess: boolean;

  constructor(private apiService: ConnectApiService) {
    this.getIssues();
  }


  ngOnInit(): void {

  }

  getIssues() {
    this.apiService.connect({
      method: 'GET',
      url: environment.endpoints.issues,
      headers: new HttpHeaders({})
    }).subscribe((response: Issue) => {
      setTimeout(() => {
        this.isAjaxSuccess = true;
        this.issueList = response.items;
        this.issueFacets = response.facets;
      }, 2000);

    }, (error) => {
      this.msgs = [{ summary: 'Network Status', detail: 'It seems server is down' }];
      this.isAjaxSuccess = false;
    });
  }

}
