import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// componeents
import { AppComponent } from './app.component';
import { HeaderComponent } from './widgets/header/header.component';
import { MainComponent } from './widgets/main/main.component';

// Prime Ng imports
import { MenubarModule } from 'primeng/menubar';
import { MenuModule } from 'primeng/menu';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ToolbarModule } from 'primeng/toolbar';
import { AvatarModule } from 'primeng/avatar';
import { BadgeModule } from 'primeng/badge';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { BpTableComponent } from './widgets/bp-table/bp-table.component';
import { BpTabsComponent } from './widgets/bp-tabs/bp-tabs.component';
import { BpChartComponent } from './widgets/bp-chart/bp-chart.component';
import { ChartModule } from 'primeng/chart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MessagesModule } from 'primeng/messages';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    BpTableComponent,
    BpTabsComponent,
    BpChartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MenubarModule,
    CardModule,
    InputTextModule,
    ButtonModule,
    ToolbarModule,
    AvatarModule,
    BadgeModule,
    TableModule,
    TabViewModule,
    MenuModule,
    ChartModule,
    OverlayPanelModule,
    ConfirmDialogModule,
    MessagesModule,
    ProgressSpinnerModule,
    DialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
