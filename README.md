# Venky-Blue-Planet

venky-blue-planet is application for get details on Issue reports using PrimeNg component.

## Installation

Use the package manager [npm](https://www.npmjs.com/package/npm) to run Blue-Planet appliction.

```bash
npm install

npm install -g @angular/cli
```

## Usage

```npm
npm run mock
ng serve

## Running mock locally
npm run mock


## Run application locally
ng serve

##
Open application http://localhost:4200

## Application hosted link
https://venky-blue-planet.web.app/